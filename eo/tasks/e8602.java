import java.util.Scanner;

public class e8602 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        if (n>=100 && n<1000000000)
            System.out.println(n/100%10);
    }
}