import java.util.Scanner;

public class e8802 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        long n = in.nextLong();
        if (n <= 9223372036854775807L)
            System.out.println(n - 1);
        else
            System.out.println("Слишком большое число");
    }
}
