import java.util.Scanner;

public class e8607 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int a= n/1000;
        int b= n/100%10;
        int c= n/10%10;
        int d= n%10;
        if (n>=1000 && n<=9999)
            System.out.println(a*a+b*b+c*c+d*d);
        else
            System.out.println("Введите четырёхзначное число");

    }
}
